#include <iostream>

#ifdef _WIN32
    #include <windows.h>
    #define sleep(n) Sleep(n)
#else
    #include <unistd.h>
#endif

#include <spdlog/spdlog.h>
#include <zmq.hpp>

#include "message.hpp"

namespace logging = spdlog;

int main()
{
    zmq::context_t context;
    zmq::socket_t socket{context, zmq::socket_type::rep};
    socket.bind("tcp://*:5555");

    while (true)
    {
        zmq::message_t request;
        socket.recv(request, zmq::recv_flags::none);

        Message req{request};
        logging::info("Received: {}", req);

        sleep(1);

        Message reply;
        reply.greeting("Back at ya");
        reply.who("Client");
        logging::info("Sending: {}", reply);
        socket.send(reply.pack(), zmq::send_flags::none);
    }
}
